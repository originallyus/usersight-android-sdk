package io.usersight.sdk.util

internal object Constants {
    const val TAG_FEEDBACK_DIALOG = "feedback-dialog"
    const val KEY_RANDOM_UUID = "device-random-uuid"
    const val SLUG_INSTALL_TIMESTAMP = "install-timestamp"
    const val SLUG_SERVER_TIMESTAMP_DELTA = "server-timestamp-delta"
    const val SLUG_METADATA = "metadata"
    const val SLUG_SDK_USER_ID = "sdk-user-id"
}