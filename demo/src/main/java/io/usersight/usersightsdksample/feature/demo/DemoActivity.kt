package io.usersight.usersightsdksample.feature.demo

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import io.usersight.sdk.BuildConfig
import io.usersight.sdk.UserSightSdk
import io.usersight.usersightsdksample.R
import io.usersight.usersightsdksample.databinding.ActivityMainBinding
import io.usersight.usersightsdksample.databinding.DialogNumberPickerBinding
import io.usersight.usersightsdksample.feature.base.BaseActivity
import io.usersight.usersightsdksample.util.Constants

class DemoActivity : BaseActivity<ActivityMainBinding>() {

    private val mLanguageTitles by lazy {
        resources.getStringArray(R.array.lang_names)
    }

    private val mLanguageCodes by lazy {
        resources.getStringArray(R.array.lang_codes)
    }

    override fun inflateViewBinding(): ActivityMainBinding = ActivityMainBinding.inflate(layoutInflater)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding?.run {
            btnChangeLanguage.setOnClickListener {
                showLanguagePickerDialog()
            }

            btnSetUserId.setOnClickListener {
                UserSightSdk.setUserId("123456810")
            }

            btnSetMetadata.setOnClickListener {
                UserSightSdk.setMetadata("policy_2371518")
            }

            btnSatisfaction.setOnClickListener {
                debugFeedbackDialog(Constants.SLUG_SATISFACTION)
            }

            btnNps.setOnClickListener {
                debugFeedbackDialog(Constants.SLUG_NPS)
            }

            btnPoll.setOnClickListener {
                debugFeedbackDialog(Constants.SLUG_POLL)
            }

            btnEffort.setOnClickListener {
                debugFeedbackDialog(Constants.SLUG_EFFORT)
            }

            btnComment.setOnClickListener {
                debugFeedbackDialog(Constants.SLUG_COMMENT)
            }

            btnExternal.setOnClickListener {
                debugFeedbackDialog(Constants.SLUG_EXTERNAL)
            }

            btnSatisfactionProduct.setOnClickListener {
                showFeedbackDialog(Constants.SLUG_SATISFACTION)
            }

            btnNpsProduct.setOnClickListener {
                showFeedbackDialog(Constants.SLUG_NPS)
            }

            btnPollProduct.setOnClickListener {
                showFeedbackDialog(Constants.SLUG_POLL)
            }

            btnCommentProduct.setOnClickListener {
                showFeedbackDialog(Constants.SLUG_COMMENT)
            }

            btnEffortProduct.setOnClickListener {
                showFeedbackDialog(Constants.SLUG_EFFORT)
            }

            btnExternalProduct.setOnClickListener {
                showFeedbackDialog(Constants.SLUG_EXTERNAL)
            }

            tvVersion.text = "Version ${BuildConfig.VERSION_NAME}"
        }
    }

    private fun debugFeedbackDialog(slug: String, eventTag: String? = null) {
        //This will always show the rating form in DEBUG mode
        //Optional: eventTag is optional
        UserSightSdk.showFeedbackDialog(this, slug, eventTag, true)

        /* Sample code for force dismissing UI, to be used only for session timeout or session expired scenarios */
        /*
         * Forcefully dismiss the rating UI
         * This may be suitable for scenarios like session timeout, session expired where host application
         * needs to forcefully dismiss any user-related UI and logout the user
         * Note: This is not part of the initial UI/UX design and business requirement. This will interfere with the
         * showing and prompting frequency logics in the backend. Please consult your business team before deciding to use this
         */
        /*
        FeedbackSdk.forceDismiss(this);
         */
    }

    private fun showFeedbackDialog(slug: String, eventTag: String? = null) {
        //A form may not be shown all the time, depends on the frequency configurations on our SDK backend.
        //Optional: eventTag is optional
        UserSightSdk.showFeedbackDialog(this, slug, eventTag, false)
    }

    private fun showLanguagePickerDialog() {
        val dialogBinding = DialogNumberPickerBinding.inflate(layoutInflater)
        val builder = AlertDialog.Builder(this)
        builder.setView(dialogBinding.root)
        dialogBinding.picker.apply {
            maxValue = mLanguageTitles.size - 1
            minValue = 0
            value = mLanguageCodes.indexOf(UserSightSdk.getCurrentLanguage()).let { if (it < 0) 0 else it }
            displayedValues = mLanguageTitles
        }
        builder.setPositiveButton(android.R.string.ok) { dialog, which ->
            dialog.dismiss()
            mLanguageCodes.getOrNull(dialogBinding.picker.value)?.let { code ->
                //Optional: set language
                //Note: this will requires translation texts to be already available on our backend
                //Language codes: "en", "zh", "zh_tw", "ms", "id", "ta", "th", "tl", "vi"
                UserSightSdk.setLanguage(code)
            }
        }
        builder.create().show()
    }
}
