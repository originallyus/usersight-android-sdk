package io.usersight.sdk.manager

import com.orhanobut.hawk.Hawk
import java.util.concurrent.TimeUnit

internal object DateTimeManager {

    private const val SLUG_DIFFERENT_TIMESTAMP_FROM_SERVER = "different-timestamp-from-server"

    fun saveServerTimestamp(timestamp: Long?) {
        timestamp?.let { serverTimestamp ->
            val deviceTimestamp = System.currentTimeMillis().toSeconds()
            val different = serverTimestamp - deviceTimestamp
            Hawk.put(SLUG_DIFFERENT_TIMESTAMP_FROM_SERVER, different)
        }
    }

    fun getCurrentTimestamp(): Long {
        val different = Hawk.get<Long>(SLUG_DIFFERENT_TIMESTAMP_FROM_SERVER, 0) ?: 0
        return System.currentTimeMillis().toSeconds() + different
    }

    fun getCurrentTimestampInMinute(): Long {
        return (getCurrentTimestamp() + 1234) / 60
    }

    private fun Long.toSeconds() = TimeUnit.MILLISECONDS.toSeconds(this)

    private fun Long.toMillis() = TimeUnit.SECONDS.toMillis(this)

}