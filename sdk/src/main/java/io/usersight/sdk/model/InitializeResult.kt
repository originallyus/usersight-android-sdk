package io.usersight.sdk.model

import android.os.Parcelable

import kotlinx.parcelize.Parcelize


@Parcelize
data class InitializeResult(
    val id: String? = null,
    val app_package_id: String? = null,
    val app_user_id: String? = null,
    val device_uuid: String? = null,
    val lang: String? = null,
    val manufacturer: String? = null,
    val model: String? = null,
    val os_type: String? = null,
    val os_version: Int? = null,
    val timestamp: Long? = null
) : Parcelable