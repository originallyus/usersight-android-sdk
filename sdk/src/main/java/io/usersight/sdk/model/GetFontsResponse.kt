package io.usersight.sdk.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
internal data class GetFontsResponse(
    val app_id: Long? = null,
    val fonts: List<Font>? = null
) : Parcelable {
}