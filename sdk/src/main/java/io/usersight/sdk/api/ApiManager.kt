package io.usersight.sdk.api

import android.os.Build
import io.usersight.sdk.BuildConfig
import io.usersight.sdk.UserSightSdk
import io.usersight.sdk.model.Form
import io.usersight.sdk.model.GetFontsResponse
import io.usersight.sdk.model.InitializeResult
import io.usersight.sdk.model.LogEventResult
import io.usersight.sdk.util.CacheUtils
import io.usersight.sdk.util.Constants
import java.util.concurrent.TimeUnit

internal object ApiManager : BaseApiManager() {

    override suspend fun getStandardHeaders(): HashMap<String, String> =
        hashMapOf<String, String>().apply {
            put("X-Device-Uuid", CacheUtils.getDeviceUUID())
            put("X-OS", "android")
            put("X-Sdk-Platform", "android")
            put("X-Version", "0.3.0")
            put("X-OS-Version", Build.VERSION.RELEASE)
            put("X-App-Version", BuildConfig.VERSION_NAME)
            put("X-Build-Number", BuildConfig.VERSION_CODE.toString())
            put("X-App-Secret", UserSightSdk.mAppSecret ?: "")
            put("X-Package-Id", UserSightSdk.mAppPackageName ?: "")
            put("X-App-User-Id", CacheUtils.getUserId())
            put("X-Lang", CacheUtils.getLanguageCode())
            put("X-Device-Manufacturer", Build.MANUFACTURER)
            put("X-Device-Model", Build.MODEL)
        }

    override suspend fun getStandardParams(): HashMap<String, String> = hashMapOf(
        "install_timestamp" to "${CacheUtils.getObject<Long>(Constants.SLUG_INSTALL_TIMESTAMP) ?: 0}"
    ).also {
        if (CacheUtils.getObject<Long>(Constants.SLUG_INSTALL_TIMESTAMP) == null)
            CacheUtils.saveObject(
                Constants.SLUG_INSTALL_TIMESTAMP,
                TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())
            )
    }

    suspend fun requestForm(slug: String, debug: Int, eventTag: String?) = postGeneric<Form>(
        "8vs2qwFD1rlSFWIa13Wc",
        hashMapOf<String, String>(),
        hashMapOf("slug" to slug, "debug" to debug.toString()).apply {
            eventTag?.let { put("event_tag", it) }
        }.toRequestJson(), Form::class.java
    )

    suspend fun submitForm(
        formId: Long,
        slug: String,
        rating: Int? = null,
        selectedOptions: String? = null,
        freeText: String? = null,
        debug: Int,
        auto: Boolean,
        eventTag: String?
    ) = postGeneric<Form>(
        "LXvcFSTTOqxxmJX9qQar",
        hashMapOf<String, String>(),
        hashMapOf(
            "request_id" to formId.toString(),
            "slug" to slug,
            "debug" to debug.toString(),
            "auto" to if (auto) "1" else "0",
        ).apply {
            eventTag?.let { put("event_tag", it) }
            if (!selectedOptions.isNullOrBlank())
                put("selected_options", selectedOptions)
            if (!freeText.isNullOrBlank())
                put("free_text", freeText)
            if (rating != null)
                put("rating", rating.toString())
        }.toRequestJson(), Form::class.java
    )

    suspend fun downloadFile(fileUrl: String) = downloadGeneric(fileUrl)

    suspend fun getPreloadFonts() = postGeneric<GetFontsResponse>(
        "JyrS9vA9Va4vI7HOhMgK",
        hashMapOf(),
        hashMapOf<String, String>().toRequestJson(),
        GetFontsResponse::class.java
    )

    suspend fun initialize() = postGeneric<InitializeResult>(
        "Vc6OIxbIMqErLLL36mhs",
        hashMapOf(),
        hashMapOf<String, String>().toRequestJson(),
        InitializeResult::class.java
    )

    suspend fun logAnalyticsEvent(sdkUserId: String, eventName: String, eventParams: Map<String, String>? = null) =
        postGeneric<LogEventResult>(
            "asy0Lm7f2tj6VxnnweKJ",
            hashMapOf(),
            hashMapOf<String, String>().apply {
                put("user_id", sdkUserId)
                put("event_name", eventName)
                if (!eventParams.isNullOrEmpty())
                    put("custom_params", mGson.toJson(eventParams))
            }.toRequestJson(),
            LogEventResult::class.java
        )
}