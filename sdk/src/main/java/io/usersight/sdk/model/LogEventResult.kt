package io.usersight.sdk.model

data class LogEventResult(
    val success: Boolean? = null,
    val timestamp: Long? = null
) {
}