package io.usersight.usersightsdksample.util.behavior

import androidx.databinding.ViewDataBinding

interface ActivityDataBindingBehavior<T : ViewDataBinding> {

    var mBinding: T?

    fun inflateViewBinding(): T

    fun onDestroy() {
        mBinding = null
    }
}