# UserSightSDK
Mobile Intrusive style             |  Mobile Non-intrusive style
:-------------------------:|:-------------------------:
![](https://gitlab.com/originallyus/usersight-android-sdk/-/raw/master/screenshot/screenshot_mobile_intrusive.png)  |  ![](https://gitlab.com/originallyus/usersight-android-sdk/-/raw/master/screenshot/screenshot_mobile_non_intrusive.png)

Tablet Intrusive style             |  Tablet Non-intrusive style
:-------------------------:|:-------------------------:
![](https://gitlab.com/originallyus/usersight-android-sdk/-/raw/master/screenshot/screenshot_tablet_intrusive.png)  |  ![](https://gitlab.com/originallyus/usersight-android-sdk/-/raw/master/screenshot/screenshot_tablet_non_intrusive.png)

## Installation
Add the dependency in your `build.gradle`
```groovy
dependencies {
    implementation 'io.usersight.sdk:usersightsdk:1.0.0'
}
```

## Initialize
The SDK needs to be initialized with an secret key specific to your app Bundle ID. Please contact us to obtain App Secret specific to your app.

Initialize the SDK in onCreate() Method of application class:
```kotlin
UserSightSdk.init(application = this, appSecret = "app-secret")
```

## Usage
Feedback form can be triggered from any where in your project code. It will stay always-on-top of your current activity.

A specific custom form can be pre-setup by business/admin via backend and to be used inside app at specific part of the app. To show such form, please add extra **formSlug** parameter to the function call.

In **Debug mode** (debug = true), the feedback forms are always shown, for debugging purpose. **DO NOT** use this function in Production/UAT/SIT environment.

In **Production mode** (debug = false), the feedback form may not be shown all the time, ie. nothing happens when the code is executed. This is not a bug. The forms are configured to be shown only once per hour, per day, per week, per quarter, etc. from backend.
```kotlin
UserSightSdk.showFeedbackDialog(activity = this, formSlug = "form-slug", eventTag = "event-tag", debug = true)
```

## Language
[Optional] Setup language used by the SDK. Supported languages:
- `en` - English (default)
- `zh` - Simplified Chinese
- `zh_tw` - Traditional Chinese
- `ms` - Malay
- `id` - Bahasa Indonesia
- `ta` - Tamil
- `th` - Thai
- `tl` - Tagalog
- `vi` - Vietnamese
```kotlin
UserSightSdk.setLanguage(languageCode = "en")
```

## UserID
[Optional] Tag an app-specific UserID to the SDK. This is only for reporting purpose.
```kotlin
UserSightSdk.setUserId(userId = "user-id")
```

## Metadata
[Optional] Tag an app-specific metadata to the SDK. This is only for reporting purpose. Please contact your business team for specific requirements.
```kotlin
UserSightSdk.setMetadata(metadata = "metadata")
```

## License
Copyright 2021 originallyus

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
