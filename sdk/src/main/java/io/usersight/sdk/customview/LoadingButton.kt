package io.usersight.sdk.customview

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.databinding.BindingAdapter
import io.usersight.sdk.BR
import io.usersight.sdk.databinding.ViewLoadingButtonBinding
import io.usersight.sdk.model.Theme
import io.usersight.sdk.util.extension.*

internal class LoadingButton : BaseCustomView<ViewLoadingButtonBinding> {

    companion object {
        @JvmStatic
        @BindingAdapter("buttonText")
        fun LoadingButton.setButtonText(text: String?) {
            mButtonText = text
        }

        @JvmStatic
        @BindingAdapter("buttonTheme")
        fun LoadingButton.applyTheme(theme: Theme?) {
            mTheme = theme
        }

        @JvmStatic
        @BindingAdapter("buttonLoading")
        fun LoadingButton.setLoading(loading: Boolean?) {
            mIsLoading = loading ?: false
        }

        @JvmStatic
        @BindingAdapter("buttonEnable")
        fun LoadingButton.setEnable(enable: Boolean?) {
            mEnable = enable ?: true
        }
    }

    var mButtonText: String? = null
        set(value) {
            field = value
            mBinding.apply {
                text = value
                notifyPropertyChanged(BR.text)
            }
        }

    var mIsLoading: Boolean = false
        set(value) {
            field = value
            mBinding.apply {
                isLoading = value
                notifyPropertyChanged(BR.isLoading)
                setButtonClickEnable(!value)
            }
        }

    var mEnable: Boolean = true
        set(value) {
            field = value
            mBinding.apply {
                enable = value
                notifyPropertyChanged(BR.enable)
                setButtonClickEnable(value)
            }
        }

    var mTheme: Theme? = null
        set(value) {
            field = value
            mBinding.btnAction.apply {
                setBackgroundColorHexForDrawable(value?.button_bg_color)
                setTextColorHex(value?.button_text_color)
                value?.button_fontsize?.let { setFontSizeSP(it) }
                value?.button_font?.let { setFontWithName(it) }
            }
        }

    private var mCurrentClickListener: OnClickListener? = null

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes)

    override fun inflateViewBinding(layoutInflater: LayoutInflater): ViewLoadingButtonBinding =
        ViewLoadingButtonBinding.inflate(layoutInflater, this, true)

    private fun setButtonClickEnable(enable: Boolean) {
        if (enable)
            mCurrentClickListener?.let { setOnClickListener(it) }
        else
            setOnClickListener(null)
    }

    override fun setOnClickListener(l: OnClickListener?) {
        l?.let { mCurrentClickListener = it }

        if (l != null)
            mBinding.btnAction.setBlinkClickAnimation().setOnClickListener(l)
        else
            mBinding.btnAction.apply {
                setOnTouchListener(null)
                setOnClickListener(null)
            }
    }
}