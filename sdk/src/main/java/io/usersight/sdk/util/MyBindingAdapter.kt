package io.usersight.sdk.util

import android.animation.Animator
import android.animation.ObjectAnimator
import android.util.Pair
import android.view.View
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.BindingAdapter
import io.usersight.sdk.util.extension.gone
import io.usersight.sdk.util.extension.load
import io.usersight.sdk.util.extension.visible

@BindingAdapter("loadImageUrl")
internal fun ImageView.loadImageUrl(url: String?) {
    load(url, width, height)
}

@BindingAdapter("app:layout_constraintWidth_percent")
internal fun View.setLayoutConstraintWidthPercent(width: Float) {
    (this.layoutParams as? ConstraintLayout.LayoutParams)?.apply {
        this.matchConstraintPercentWidth = width
    }
}

@BindingAdapter("animatedVisibility")
internal fun View.setAnimatedVisibility(newVisibility: Pair<Int, Long>) {
    if (this.visibility == newVisibility.first) return
    val willShow = newVisibility.first == View.VISIBLE
    val willHide = newVisibility.first == View.GONE
    this.animate().alpha(if (willShow) 1f else 0f).setDuration(newVisibility.second)
        .setListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator?) {
                if (willShow) this@setAnimatedVisibility.visible()
            }

            override fun onAnimationEnd(animation: Animator?) {
                if (willHide) this@setAnimatedVisibility.gone()
            }

            override fun onAnimationCancel(animation: Animator?) {
            }

            override fun onAnimationRepeat(animation: Animator?) {
            }
        })
}

@BindingAdapter("animatedRotation")
internal fun View.setAnimatedRotation(rotationAnimation: Pair<Float, Long>) {
    if (this.rotation == rotationAnimation.first) return
    ObjectAnimator.ofFloat(this, View.ROTATION, rotationAnimation.first).apply {
        duration = rotationAnimation.second
    }.start()
}