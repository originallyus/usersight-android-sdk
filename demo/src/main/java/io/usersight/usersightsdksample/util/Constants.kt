package io.usersight.usersightsdksample.util

object Constants {
    const val DEMO_APP_SECRET = "4WWCXBMYwXRkk8gbWfPu"

    const val SLUG_SATISFACTION = "satisfaction-1"
    const val SLUG_NPS = "nps-1"
    const val SLUG_POLL = "poll-1"
    const val SLUG_COMMENT = "comment-1"
    const val SLUG_EFFORT = "effort-1"
    const val SLUG_EXTERNAL = "external-1"

}