package io.usersight.usersightsdksample

import android.app.Application
import io.usersight.sdk.UserSightSdk
import io.usersight.usersightsdksample.util.Constants

class DemoApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        //Initialize FeedbackSdk
        //Please consult your business team to obtain App Secret specific to your app's Bundle ID for different environments
        UserSightSdk.init(this, Constants.DEMO_APP_SECRET)
    }
}