package io.usersight.usersightsdksample.feature.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.databinding.ViewDataBinding
import io.usersight.usersightsdksample.util.behavior.ActivityDataBindingBehavior

abstract class BaseActivity<T : ViewDataBinding> : AppCompatActivity(),
    ActivityDataBindingBehavior<T> {

    override var mBinding: T? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = inflateViewBinding()
        setContentView(mBinding?.root)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
    }

    override fun onDestroy() {
        super<AppCompatActivity>.onDestroy()
        super<ActivityDataBindingBehavior>.onDestroy()
    }
}