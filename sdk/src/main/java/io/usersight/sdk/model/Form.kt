package io.usersight.sdk.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
internal data class Form(
    val id: Long? = null,
    val alignment: String? = null,
    val title: String? = null,
    val question: String? = null,
    val button_text: String? = null,
    val comment_placeholder: String? = null,
    val comment_placeholder_5_star: String? = null,
    val debug_message: String? = null,
    val fineprint: String? = null,
    val form_format: FormFormat? = null,
    val image_file_url: String? = null,
    val instruction: String? = null,
    val instruction_5_star: String? = null,
    val instrusive_style: Int? = null,
    val ios_store_app_id: Int? = null,
    val log: String? = null,
    val options: List<Option>? = null,
    val options_multiselect: Int? = null,
    val package_id: String? = null,
    val package_id_alt: String? = null,
    val rating_max: Int? = null,
    val rating_min: Int? = null,
    val secondary_question: String? = null,
    val secondary_question_5_star: String? = null,
    val show_rating_number: Int? = null,
    val show_rating_star: Int? = null,
    val read_only_rating: Int? = null,
    val show_comment: Boolean? = null,
    val comment_mandatory: Int? = null,
    val theme: Theme? = null,
    val theme_id: Int? = null,
    val timestamp: Int? = null,
    val type: String? = null,
    val url: String? = null,
    val auto_submit: List<Int>? = null,
    val scale_label_left: String? = null,
    val scale_label_right: String? = null,
    val preload_images: List<String>? = null,
    val extra_form: Form? = null,
    val auto_store_review: Boolean? = null,
    val android_review_url: String? = null,
    val fonts: List<Font>? = null
) : Parcelable {

    fun checkIntrusiveStyle() = instrusive_style == 1

    fun checkIfLeftAlignment() = alignment == "left"

    fun checkShouldShowRatingStar() = show_rating_star == 1

    fun checkShouldShowRatingNumber() = show_rating_number == 1

    fun checkNeedAutoLaunchStoreReview() = auto_store_review == true
}

@Parcelize
data class Option(
    val id: Long? = null,
    val slug: String? = null,
    val title: String? = null
) : Parcelable

@Parcelize
data class FormFormat(
    val `000_description`: Description? = null,
    val `1_0_suggestion`: List<String>? = null,
    val notrans_url: String? = null
) : Parcelable

@Parcelize
data class Theme(
    val body_bg_color: String? = null,
    val body_text_color: String? = null,
    val button_bg_color: String? = null,
    val button_font: String? = null,
    val button_fontsize: Int? = null,
    val button_text_color: String? = null,
    val close_icon_color: String? = null,
    val fineprint_color: String? = null,
    val fineprint_font: String? = null,
    val fineprint_fontsize: Int? = null,
    val id: Int? = null,
    val instruction_color: String? = null,
    val instruction_font: String? = null,
    val instruction_fontsize: Int? = null,
    val name: String? = null,
    val normal_star_color: String? = null,
    val option_bg_color: String? = null,
    val option_border_color: String? = null,
    val option_font: String? = null,
    val option_fontsize: Int? = null,
    val option_selected_bg_color: String? = null,
    val option_selected_border_color: String? = null,
    val option_selected_text_color: String? = null,
    val option_text_color: String? = null,
    val question_color: String? = null,
    val question_font: String? = null,
    val question_fontsize: Int? = null,
    val rating_number_bg_color: String? = null,
    val rating_number_border_color: String? = null,
    val rating_number_font: String? = null,
    val rating_number_fontsize: Int? = null,
    val rating_number_selected_bg_color: String? = null,
    val rating_number_selected_border_color: String? = null,
    val rating_number_selected_text_color: String? = null,
    val rating_number_text_color: String? = null,
    val scale_label_color: String? = null,
    val scale_label_font: String? = null,
    val scale_label_fontsize: Int? = null,
    val secondary_question_color: String? = null,
    val secondary_question_font: String? = null,
    val secondary_question_fontsize: Int? = null,
    val selected_star_color: String? = null,
    val textarea_bg_color: String? = null,
    val textarea_border_color: String? = null,
    val textarea_focus_bg_color: String? = null,
    val textarea_focus_border_color: String? = null,
    val textarea_font: String? = null,
    val textarea_fontsize: Int? = null,
    val textarea_placeholder_color: String? = null,
    val textarea_text_color: String? = null,
    val title_color: String? = null,
    val title_font: String? = null,
    val title_fontsize: Int? = null
) : Parcelable

@Parcelize
data class Description(
    val en: String? = null
) : Parcelable

@Parcelize
data class Font(
    val id: Long? = null,
    val name: String? = null,
    val file: String? = null,
    val file_url: String? = null
) : Parcelable