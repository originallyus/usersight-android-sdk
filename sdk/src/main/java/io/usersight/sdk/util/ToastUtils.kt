package io.usersight.sdk.util

import android.content.Context
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.muddzdev.styleabletoast.StyleableToast
import io.usersight.sdk.R
import io.usersight.sdk.util.extension.isUnavailable

/**
 * Created by originally.us on 5/29/14.
 */
internal object ToastUtils {

    fun showSuccess(context: Context?, message: String?, duration: Int = Toast.LENGTH_SHORT) {
        if (message.isNullOrBlank()) return
        context?.takeUnless { it.isUnavailable() }?.let {
            StyleableToast.Builder(it)
                .text(message)
                .length(duration)
                .textColor(R.color.white.getColorFromResId(it))
                .cornerRadius(R.dimen.toast_radius.getDimensionFromResId(it))
                .backgroundColor(R.color.toast_success.getColorFromResId(it))
                .show()
        }
    }

    fun showError(context: Context?, message: String?, duration: Int = Toast.LENGTH_SHORT) {
        if (message.isNullOrBlank()) return
        context?.takeUnless { it.isUnavailable() }?.let {
            StyleableToast.Builder(it)
                .text(message)
                .length(duration)
                .textColor(R.color.white.getColorFromResId(it))
                .cornerRadius(R.dimen.toast_radius.getDimensionFromResId(it))
                .backgroundColor(R.color.toast_error.getColorFromResId(it))
                .show()
        }
    }

    fun showInfo(context: Context?, message: String?, duration: Int = Toast.LENGTH_SHORT) {
        if (message.isNullOrBlank()) return
        context?.takeUnless { it.isUnavailable() }?.let {
            StyleableToast.Builder(it)
                .text(message)
                .length(duration)
                .textColor(R.color.white.getColorFromResId(it))
                .cornerRadius(R.dimen.toast_radius.getDimensionFromResId(it))
                .backgroundColor(R.color.toast_info.getColorFromResId(it))
                .show()
        }
    }

    fun showWarning(context: Context?, message: String?, duration: Int = Toast.LENGTH_SHORT) {
        if (message.isNullOrBlank()) return
        context?.takeUnless { it.isUnavailable() }?.let {
            StyleableToast.Builder(it)
                .text(message)
                .length(duration)
                .textColor(R.color.white.getColorFromResId(it))
                .cornerRadius(R.dimen.toast_radius.getDimensionFromResId(it))
                .backgroundColor(R.color.toast_warning.getColorFromResId(it))
                .show()
        }
    }

    private fun Int.getDimensionFromResId(context: Context) = context.resources.getDimensionPixelSize(this)

    private fun Int.getColorFromResId(context: Context) = ContextCompat.getColor(context, this)
}
