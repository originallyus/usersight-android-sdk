package io.usersight.sdk.util.extension

import android.app.Activity
import android.content.Context
import androidx.core.app.NotificationManagerCompat
import io.usersight.sdk.R

internal fun Context?.isUnavailable() = this == null
        || (this as? Activity)?.isFinishing == true
        || (this as? Activity)?.isDestroyed == true

internal fun Context?.isThisATablet() = this?.resources?.getBoolean(R.bool.isTablet) == true

internal fun Context?.getScreenWidth() = this?.resources?.displayMetrics?.widthPixels ?: 0

internal fun Context?.getScreenHeight() = this?.resources?.displayMetrics?.heightPixels ?: 0

internal fun Context?.isNotificationEnabled() = this?.let { NotificationManagerCompat.from(it).areNotificationsEnabled() }
        ?: false