package io.usersight.sdk.api

import com.google.gson.GsonBuilder
import com.google.gson.JsonElement
import okhttp3.CertificatePinner
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

@JvmSuppressWildcards
internal interface BaseApiService {

    companion object {
        private const val PRODUCTION_URL = "https://usersight.io"
        const val GENERIC_URL = "{url}"

        fun create(customBaseUrl: String = ""): BaseApiService {
            val headers = getHeader()
            val gson = GsonBuilder().apply {
                setDateFormat("yyyy-MM-dd")
            }.create()

            return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(getBaseUrl(customBaseUrl))
                .client(headers)
                .build().create(BaseApiService::class.java)
        }

        private fun getBaseUrl(customBaseUrl: String = ""): String {
            if (customBaseUrl.isNotBlank()) return customBaseUrl
            return PRODUCTION_URL
        }

        private fun getHeader(): OkHttpClient {
            val builder = OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .certificatePinner(CertificatePinner.Builder().apply {
                    add(
                        "usersight.io",
                        "sha256/JRrKUXErnR95gnBhbSzFniqYfG4Mvjj62Pqc86ve58w=",
                        "sha256/jQJTbIh0grw0/1TkHSumWb+Fs0Ggogr621gT3PvPKG0=",
                        "sha256/C5+lpZ7tcVwmwQIMcRtPbsQtWLABXhQzejna0wHFr8M="
                    )
                }.build())
                .addInterceptor(HttpLoggingInterceptor().apply {
                    setLevel(HttpLoggingInterceptor.Level.BODY)
                })
            return builder.build()
        }
    }

    @GET(GENERIC_URL)
    suspend fun getGeneric(
        @Path(value = "url", encoded = true) url: String,
        @HeaderMap headers: Map<String, String> = HashMap(),
        @QueryMap params: Map<String, String> = HashMap()
    ): JsonElement

    @POST
    suspend fun postGeneric(
        @Url url: String,
        @HeaderMap headers: Map<String, String> = HashMap(),
        @Body params: JsonElement
    ): JsonElement

    @Streaming
    @GET
    suspend fun downloadGeneric(@Url fileUrl: String): ResponseBody
}