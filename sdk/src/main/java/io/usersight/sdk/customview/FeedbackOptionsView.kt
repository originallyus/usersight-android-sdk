package io.usersight.sdk.customview

import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.core.view.children
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import io.usersight.sdk.R
import io.usersight.sdk.databinding.ViewFeedbackOptionItemBinding
import io.usersight.sdk.databinding.ViewFeedbackOptionsBinding
import io.usersight.sdk.model.Option
import io.usersight.sdk.model.Theme
import io.usersight.sdk.util.extension.parseColorHex
import io.usersight.sdk.util.extension.setFontSizeSP
import io.usersight.sdk.util.extension.setFontWithName
import io.usersight.sdk.util.extension.setTextColorHex

internal class FeedbackOptionsView : BaseCustomView<ViewFeedbackOptionsBinding> {

    companion object {
        @JvmStatic
        @BindingAdapter("optionTheme", "optionData", "optionAllowMultiSelect")
        fun FeedbackOptionsView.setData(theme: Theme?, data: List<Option>?, allowMultiSelect: Boolean?) {
            mTheme = theme
            mAllowMultiSelect = allowMultiSelect ?: false
            mData = data
            rebuildOptionsView()
        }
    }

    var mAllowMultiSelect: Boolean = false

    var mTheme: Theme? = null
        set(value) {
            field = value
            mBinding.layoutOptions.children.forEach {
                DataBindingUtil.bind<ViewFeedbackOptionItemBinding>(it)?.tvOptionName?.applyOptionTheme()
            }
        }

    var mData: List<Option>? = null

    var mSelectedSlugs: List<String>? = null
        set(value) {
            field = value
            reloadSelectedState()
            mOnSelectedOptionsChanged?.invoke(value)
        }

    var mOnSelectedOptionsChanged: ((List<String>?) -> Unit)? = null

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes)

    override fun inflateViewBinding(layoutInflater: LayoutInflater): ViewFeedbackOptionsBinding =
        ViewFeedbackOptionsBinding.inflate(layoutInflater, this, true)

    override fun initialize(attrs: AttributeSet?) {
        super.initialize(attrs)
        rebuildOptionsView()
    }

    private fun rebuildOptionsView() {
        mBinding.layoutOptions.apply {
            removeAllViews()
            val layoutInflater = LayoutInflater.from(context)
            mData?.forEach { option ->
                val optionItemView = layoutInflater.inflate(R.layout.view_feedback_option_item, null)
                DataBindingUtil.bind<ViewFeedbackOptionItemBinding>(optionItemView)?.run {
                    tvOptionName.apply {
                        text = option.title
                        tag = option.slug
                        isSelected = mSelectedSlugs?.contains(option.slug) == true
                        applyOptionTheme()
                        setOnClickListener {
                            option.slug?.let {
                                mSelectedSlugs = if (mAllowMultiSelect) {
                                    val currentSlugs = mSelectedSlugs?.toMutableList() ?: mutableListOf()
                                    if (isSelected)
                                        currentSlugs.remove(it)
                                    else
                                        currentSlugs.add(it)
                                    currentSlugs
                                } else
                                    mutableListOf(it)
                            }
                        }
                    }
                }
                this.addView(optionItemView, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            }
        }
    }

    private fun reloadSelectedState() {
        mBinding.layoutOptions.apply {
            children.forEach { optionView ->
                optionView.apply {
                    this.isSelected = mSelectedSlugs?.contains(this.tag as? String) == true
                    (this as? AppCompatTextView)?.applyOptionTheme()
                }
            }
        }
    }

    private fun AppCompatTextView.applyOptionTheme() {
        val strTextColor = if (isSelected) mTheme?.option_selected_text_color else mTheme?.option_text_color
        setTextColorHex(strTextColor, R.color.color_text_dark_grey)

        mTheme?.option_fontsize?.let { setFontSizeSP(it) }

        mTheme?.option_font?.let { setFontWithName(it) }

        val strBgColor = if (isSelected) mTheme?.option_selected_bg_color else mTheme?.option_bg_color
        val strBorderColor = if (isSelected) mTheme?.option_selected_border_color else mTheme?.option_border_color
        (background as? GradientDrawable)?.apply {
            mutate()
            this.setStroke(resources.getDimensionPixelSize(R.dimen.default_line_height), strBorderColor?.parseColorHex() ?: ContextCompat.getColor(context, R.color.line_color))
            this.setColor(strBgColor?.parseColorHex() ?: ContextCompat.getColor(context, R.color.white))
        }
    }

}