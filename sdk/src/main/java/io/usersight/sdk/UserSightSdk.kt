package io.usersight.sdk

import android.animation.Animator
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.orhanobut.hawk.Hawk
import io.usersight.sdk.api.ApiManager
import io.usersight.sdk.api.ApiResult
import io.usersight.sdk.customview.FeedbackFormContentView
import io.usersight.sdk.manager.InAppReviewManager
import io.usersight.sdk.model.Font
import io.usersight.sdk.model.Form
import io.usersight.sdk.util.CacheUtils
import io.usersight.sdk.util.Constants
import io.usersight.sdk.util.GlideApp
import io.usersight.sdk.util.ToastUtils
import io.usersight.sdk.util.extension.*
import kotlinx.coroutines.*
import timber.log.Timber
import java.io.File
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

@SuppressLint("StaticFieldLeak")
object UserSightSdk : Application.ActivityLifecycleCallbacks {

    internal var mAppPackageName: String? = null

    internal var mAppSecret: String? = null

    internal var mCurrentFeedbackDialogView: View? = null

    internal var mCurrentFeedbackDialogData: Form? = null

    private var mRequestFormJob: Job? = null

    private var mSubmitFormJob: Job? = null

    private const val SLIDE_ANIMATION_DURATION = 300L

    fun init(application: Application, appSecret: String) {
        mAppPackageName = application.packageName
        mAppSecret = appSecret
        application.registerActivityLifecycleCallbacks(this)
        Hawk.init(application.applicationContext).build()
        Timber.plant(Timber.DebugTree())
        CacheUtils.saveDeviceUUID(application.applicationContext)


        GlobalScope.launch {
            // Initialize to get sdk user id
            val initializeResult = ApiManager.initialize()
            if (initializeResult is ApiResult.Success) {
                initializeResult.data?.id?.let { sdkUserId ->
                    CacheUtils.saveSdkUserId(sdkUserId)
                }
            }

            // Preload resources
            val result = ApiManager.getPreloadFonts()
            if (result is ApiResult.Success) {
                result.data?.fonts?.filterNot { it.file_url.isNullOrBlank() }?.forEach { font ->
                    launch(Dispatchers.IO) {
                        preloadFont(application, font).let { preloadResult ->
                            if (preloadResult.second)
                                Timber.d("Preload succeed: ${font.file_url}")
                            else
                                Timber.d("Preload failed: ${font.file_url}")
                        }
                    }
                }
            }
        }
    }

    internal fun logAnalyticsEvent(eventName: String, eventParams: Map<String, String>?) {
        CacheUtils.getSdkUserId().takeIf { !it.isNullOrBlank() }?.let { sdkUserId ->
            GlobalScope.launch {
                ApiManager.logAnalyticsEvent(sdkUserId, eventName, eventParams)
            }
        }
    }

    fun setLanguage(languageCode: String) {
        if (mCurrentFeedbackDialogView != null) return
        CacheUtils.saveLanguageCode(languageCode)
    }

    fun getCurrentLanguage() = CacheUtils.getLanguageCode()

    fun setUserId(userId: String?) {
        if (mCurrentFeedbackDialogView != null) return
        CacheUtils.saveUserId(userId)
    }

    fun getCurrentUserId() = CacheUtils.getUserId()

    fun setMetadata(metadata: String?) {
        if (mCurrentFeedbackDialogView != null) return
        CacheUtils.saveObject(Constants.SLUG_METADATA, metadata)
    }

    fun getCurrentMetadata() = CacheUtils.getObject<String>(Constants.SLUG_METADATA)

    /*
     * Forcefully dismiss the rating UI
     * This may be suitable for scenarios like session timeout, session expired where host application
     * needs to forcefully dismiss any user-related UI and logout the user
     * Note: This is not part of the initial UI/UX design and business requirement. This will interfere with the
     * showing and prompting frequency logics in the backend. Please consult your business team before deciding to use this
     */
    fun forceDismiss(activity: Activity) {
        closeCurrentFloatingView(activity)
    }

    fun showFeedbackDialog(
        activity: Activity, formSlug: String, eventTag: String?, debug: Boolean
    ) {
        // Check exiting dialog view
        if (activity.getContentViewGroup()
                ?.findViewWithTag<FeedbackFormContentView>(Constants.TAG_FEEDBACK_DIALOG) != null
        ) return

        // Check exiting request form
        if (mRequestFormJob?.isActive == true) return

        // Log
        logAnalyticsEvent("request_form", hashMapOf<String, String>().apply {
            put("form_slug", formSlug)
            eventTag?.let { put("event_tag", it) }
        })

        val activityScope = (activity as? AppCompatActivity)?.lifecycleScope
        val inTabletMode = activity.isThisATablet()
        mRequestFormJob = activityScope?.launch {
            when (val result = ApiManager.requestForm(formSlug, if (debug) 1 else 0, eventTag)) {
                is ApiResult.Success -> {
                    mCurrentFeedbackDialogData = result.data ?: return@launch
                    if (mCurrentFeedbackDialogData?.title.isNullOrBlank() || mCurrentFeedbackDialogData?.theme == null) return@launch
                    val isIntrusiveStyle = result.data?.checkIntrusiveStyle() == true

                    // Build
                    mCurrentFeedbackDialogView = FeedbackFormContentView(activity).apply {
                        id = View.generateViewId()
                        tag = Constants.TAG_FEEDBACK_DIALOG
                        mIsSubmittedForm = false
                        mIsUsingExtraForm = false
                        mInTabletMode = inTabletMode
                        mIsCollapsed = inTabletMode && !isIntrusiveStyle
                        mIsSubmitting = false
                        mData = result.data
                        mListener = object : FeedbackFormContentView.Listener {
                            override fun onLaunchUrlClick(url: String) {
                                url.openUrlWithCustomTabs(activity)
                            }

                            override fun onCloseDialogClick() {
                                mSubmitFormJob?.cancel()
                                closeCurrentFloatingView(activity)
                            }

                            override fun onShowOriginalFormClick() {
                                this@apply.mIsUsingExtraForm = false
                                this@apply.mData = mCurrentFeedbackDialogData
                                reloadViewsVisibility(true)
                            }

                            override fun onGoToStoreReviewClick(storeUrl: String) {
                                if (this@apply.mIsUsingExtraForm) {
                                    this@apply.apply {
                                        mIsUsingExtraForm = false
                                        mData = mCurrentFeedbackDialogData
                                        reloadViewsVisibility(true)
                                    }
                                } else
                                    closeCurrentFloatingView(activity)

                                try {
                                    activity.startActivity(
                                        Intent(
                                            Intent.ACTION_VIEW,
                                            Uri.parse(storeUrl)
                                        )
                                    )
                                } catch (e: Exception) {
                                    Timber.e(e)
                                    ToastUtils.showError(context, e.message)
                                    return
                                }
                            }

                            override fun onSubmitClick(
                                starRating: Int?,
                                numberRating: Int?,
                                selectedOptions: String?,
                                comment: String?,
                                isAutoSubmit: Boolean
                            ) {
                                val formId = mCurrentFeedbackDialogData?.id ?: return
                                val rating = when {
                                    mCurrentFeedbackDialogData?.checkShouldShowRatingStar() == true -> starRating
                                        ?: 0
                                    mCurrentFeedbackDialogData?.checkShouldShowRatingNumber() == true -> numberRating
                                        ?: 0
                                    else -> 0
                                }
                                mSubmitFormJob = activityScope.launch {
                                    this@apply.mIsSubmitting = true
                                    val submitResult = ApiManager.submitForm(
                                        formId,
                                        formSlug,
                                        rating,
                                        selectedOptions,
                                        comment,
                                        if (debug) 1 else 0,
                                        isAutoSubmit,
                                        eventTag
                                    )
                                    if (mSubmitFormJob?.isActive != true) return@launch
                                    when (submitResult) {
                                        is ApiResult.Success -> {
                                            // Set submit result
                                            mCurrentFeedbackDialogData = submitResult.data
                                            this@apply.apply {
                                                mIsSubmitting = false
                                                mIsSubmittedForm = true
                                                mIsUsingExtraForm =
                                                    submitResult.data?.extra_form != null
                                                mData =
                                                    if (mIsUsingExtraForm) submitResult.data?.extra_form else submitResult.data
                                                reloadViewsVisibility(true)
                                            }

                                            // Show auto rating if needed
                                            if (submitResult.data?.checkNeedAutoLaunchStoreReview() == true) {
                                                InAppReviewManager.startInAppReviewFlow(activity)
                                            }
                                        }
                                        is ApiResult.Failure -> {
                                            this@apply.mIsSubmitting = false
                                            Timber.e(submitResult.error)
                                            ToastUtils.showError(
                                                activity,
                                                submitResult.error.message
                                            )
                                        }
                                    }
                                }
                            }
                        }
                        reloadViewsVisibility(false)
                    }

                    // Add
                    if (inTabletMode) {
                        val dialogWidth = (activity.getScreenWidth()
                            .toFloat() * (if (isIntrusiveStyle) 0.6f else 0.3f)).toInt()
                        activity.addContentView(mCurrentFeedbackDialogView,
                            FrameLayout.LayoutParams(
                                dialogWidth,
                                FrameLayout.LayoutParams.WRAP_CONTENT
                            ).apply {
                                gravity = if (isIntrusiveStyle) Gravity.CENTER else Gravity.BOTTOM
                            })
                    } else {
                        activity.addContentView(mCurrentFeedbackDialogView,
                            FrameLayout.LayoutParams(
                                FrameLayout.LayoutParams.MATCH_PARENT, if (isIntrusiveStyle)
                                    FrameLayout.LayoutParams.MATCH_PARENT
                                else
                                    FrameLayout.LayoutParams.WRAP_CONTENT
                            ).apply {
                                gravity = Gravity.BOTTOM
                            })
                    }

                    // Preload images if any
                    val preloadImages = mutableListOf<String>().apply {
                        result.data?.image_file_url?.let { add(it) }
                        addAll(result.data?.preload_images ?: emptyList())
                    }
                    val preloadImagesTasks = mutableListOf<Deferred<Pair<String, Boolean>>>()
                    preloadImages.forEach { imageUrl ->
                        activityScope.async(Dispatchers.IO) { preloadImage(activity, imageUrl) }
                            .let {
                                preloadImagesTasks.add(it)
                            }
                    }
                    preloadImagesTasks.awaitAll()

                    // Preload fonts if any
                    val preloadFontsTasks = mutableListOf<Deferred<Pair<Font, Boolean>>>()
                    result.data?.fonts?.forEach { font ->
                        if (font.file_url.isNullOrBlank()) return@forEach
                        activityScope.async(Dispatchers.IO) { preloadFont(activity, font) }.let {
                            preloadFontsTasks.add(it)
                        }
                    }
                    preloadFontsTasks.awaitAll()

                    // Show
                    mCurrentFeedbackDialogView?.afterMeasured {
                        this.animate().translationY(
                            getOffscreenPosition(
                                context,
                                this.height,
                                inTabletMode,
                                isIntrusiveStyle
                            )
                        ).apply {
                            // Set tablet non-intrusive x position
                            if (inTabletMode && !isIntrusiveStyle) {
                                val newX = (activity.getScreenWidth()
                                    .toFloat() * 0.75f) - (this@afterMeasured.width.toFloat() / 2f)
                                translationX(newX)
                            }
                        }.setDuration(0).start()
                        this.animate().translationY(0f).setDuration(SLIDE_ANIMATION_DURATION)
                            .start()
                    }

                }
                is ApiResult.Failure -> {
                    Timber.e(result.error)
                    if (debug) ToastUtils.showError(activity, result.error.message)
                }
            }
        }
    }

    private suspend fun preloadImage(context: Context, fileUrl: String) =
        suspendCoroutine<Pair<String, Boolean>> { continuation ->
            GlideApp.with(context).load(fileUrl)
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Drawable>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        continuation.resume(Pair(fileUrl, false))
                        return false
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: Target<Drawable>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        continuation.resume(Pair(fileUrl, true))
                        return false
                    }
                }).preload()
        }

    private suspend fun preloadFont(context: Context, font: Font): Pair<Font, Boolean> {
        if (font.file.isNullOrBlank() || font.file_url.isNullOrBlank())
            return Pair(font, false)

        // Create fonts dir if missing
        val fontsDir = File(context.filesDir, "fonts")
        if (!fontsDir.exists()) fontsDir.mkdir()

        // Check if font is existed
        val localFontFile = File(context.filesDir, "fonts/${font.file}")
        if (localFontFile.exists()) {
            return Pair(font, true)
        }

        // Download font
        return when (val result = ApiManager.downloadFile(font.file_url)) {
            is ApiResult.Success -> {
                result.data?.byteStream()?.let { input ->
                    localFontFile.outputStream().use { output ->
                        input.copyTo(output)
                    }
                }
                Pair(font, true)
            }
            is ApiResult.Failure -> {
                Pair(font, false)
            }
            else -> {
                Pair(font, false)
            }
        }
    }

    private fun getOffscreenPosition(
        context: Context,
        dialogHeight: Int,
        inTabletMode: Boolean,
        isIntrusiveStyle: Boolean
    ): Float {
        val screenHeight = context.getScreenHeight()
        var offscreenPosition = dialogHeight.toFloat()
        if (inTabletMode && isIntrusiveStyle) {
            offscreenPosition += (screenHeight - dialogHeight) / 2f
        }
        return offscreenPosition
    }

    private fun closeCurrentFloatingView(activity: Activity) {
        val dialogViewToClose = mCurrentFeedbackDialogView ?: activity.getContentViewGroup()
            ?.findViewWithTag(Constants.TAG_FEEDBACK_DIALOG)
        dialogViewToClose?.run {
            animate().translationY(
                getOffscreenPosition(
                    context,
                    this.height,
                    context.isThisATablet(),
                    mCurrentFeedbackDialogData?.checkIntrusiveStyle() == true
                )
            ).setDuration(SLIDE_ANIMATION_DURATION)
                .setListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator?, isReverse: Boolean) {

                    }

                    override fun onAnimationStart(animation: Animator?) {
                    }

                    override fun onAnimationEnd(animation: Animator?, isReverse: Boolean) {
                    }

                    override fun onAnimationEnd(animation: Animator?) {
                        activity.getContentViewGroup()?.removeView(this@run)
                    }

                    override fun onAnimationCancel(animation: Animator?) {
                    }

                    override fun onAnimationRepeat(animation: Animator?) {
                    }
                })
                .start()
        }
        mCurrentFeedbackDialogView = null
    }

    private fun Activity.getContentViewGroup(): ViewGroup? = this.findViewById(android.R.id.content)

    override fun onActivityPaused(activity: Activity) {
    }

    override fun onActivityStarted(activity: Activity) {
    }

    override fun onActivityDestroyed(activity: Activity) {
        closeCurrentFloatingView(activity)
    }

    override fun onActivitySaveInstanceState(activity: Activity, p1: Bundle) {
    }

    override fun onActivityStopped(activity: Activity) {
    }

    override fun onActivityCreated(activity: Activity, p1: Bundle?) {
    }

    override fun onActivityResumed(activity: Activity) {
    }
}