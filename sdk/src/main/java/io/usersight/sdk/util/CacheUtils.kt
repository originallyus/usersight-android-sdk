package io.usersight.sdk.util

import android.content.Context
import com.orhanobut.hawk.Hawk

internal object CacheUtils {

    private const val SLUG_APP_USER_ID = "app-user-id"

    private const val SLUG_LANGUAGE_CODE = "lang-code"

    private const val SLUG_DEVICE_UUID = "device-uuid"

    inline fun <reified T> getObject(key: String): T? = if (Hawk.contains(key)) Hawk.get<T>(key) else null

    inline fun <reified T> saveObject(key: String, value: T) {
        Hawk.put(key, value)
    }

    fun saveUserId(userId: String?) {
        if (!userId.isNullOrBlank())
            Hawk.put(SLUG_APP_USER_ID, userId)
        else
            Hawk.delete(SLUG_APP_USER_ID)
    }

    fun getUserId() = Hawk.get<String>(SLUG_APP_USER_ID) ?: ""

    fun saveSdkUserId(id: String) {
        Hawk.put(Constants.SLUG_SDK_USER_ID, id)
    }

    fun getSdkUserId() = Hawk.get<String?>(Constants.SLUG_SDK_USER_ID)

    fun saveLanguageCode(code: String) = Hawk.put(SLUG_LANGUAGE_CODE, code)

    fun getLanguageCode() = Hawk.get<String>(SLUG_LANGUAGE_CODE) ?: ""

    fun saveDeviceUUID(context: Context) = saveObject(SLUG_DEVICE_UUID, DeviceUtils.getDeviceUUID(context))

    fun getDeviceUUID() = getObject<String>(SLUG_DEVICE_UUID) ?: ""
}