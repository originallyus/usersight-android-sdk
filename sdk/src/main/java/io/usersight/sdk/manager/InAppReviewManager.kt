package io.usersight.sdk.manager

import android.app.Activity
import com.google.android.play.core.review.ReviewManager
import com.google.android.play.core.review.ReviewManagerFactory
import timber.log.Timber

internal object InAppReviewManager {

    private lateinit var mReviewManager: ReviewManager

    fun startInAppReviewFlow(activity: Activity) {
        if (!this::mReviewManager.isInitialized)
            mReviewManager = ReviewManagerFactory.create(activity)

        mReviewManager.requestReviewFlow()
            .addOnSuccessListener { reviewInfo ->
                mReviewManager.launchReviewFlow(activity, reviewInfo)
                }
                .addOnFailureListener {
                    Timber.e(it)
                }
    }

}