package io.usersight.sdk.api

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import io.usersight.sdk.util.CacheUtils
import io.usersight.sdk.util.Constants
import io.usersight.sdk.util.extension.sha256
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.ResponseBody
import org.threeten.bp.Instant
import java.lang.reflect.Type
import kotlin.math.floor
import kotlin.random.Random

internal abstract class BaseApiManager {

    companion object {
        const val KEY_TIMESTAMP = "timestamp"
    }

    abstract suspend fun getStandardHeaders(): HashMap<String, String>

    abstract suspend fun getStandardParams(): HashMap<String, String>

    private val mRandomEncryptedSubdomainPath by lazy {
        arrayOf(
            "39908141255613216084",
            "44854569532069212172",
            "dds2cj0chsdhzanrk6y9",
            "lk04xmjn16qweucu0suw",
            "yl28ri2w8k6ug8cwsqw8"
        )
    }

    private val mRandomEncryptedBackendPath by lazy {
        arrayOf("VlAI2Q1FDHvASMhqTXe0", "rNwrNi5Iw36epJS9gprO", "Ij8Ji87IpdZTXSw1kAFV")
    }

    private fun getRandomEncryptedApiUrl() =
        "https://${mRandomEncryptedSubdomainPath[Random.nextInt(mRandomEncryptedSubdomainPath.size)]}.usersight.io/backend/${
            mRandomEncryptedBackendPath[Random.nextInt(
                mRandomEncryptedBackendPath.size
            )]
        }"

    open val mGson: Gson by lazy {
        GsonBuilder().apply {
            setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        }.create()
    }

    private val mApiService: BaseApiService by lazy {
        BaseApiService.create()
    }

    open suspend fun <T> getGeneric(
        url: String,
        customHeaders: HashMap<String, String> = hashMapOf(),
        customParams: HashMap<String, String> = hashMapOf(),
        type: Type
    ): ApiResult<T>? {
        return withContext(Dispatchers.IO) {
            try {
                ApiResultHandler.handleSuccess<T>(
                    mApiService.getGeneric(
                        url,
                        getStandardHeaders().apply { putAll(customHeaders) },
                        getStandardParams().apply { putAll(customParams) }),
                    type, mGson
                )
            } catch (e: Exception) {
                ApiResultHandler.handleException<T>(e)
            }
        }
    }

    open suspend fun <T> postGeneric(
        url: String,
        customHeaders: HashMap<String, String> = hashMapOf(),
        customParams: JsonElement,
        type: Type,
        retryOnEncryptionError: Boolean = true
    ): ApiResult<T> {
        return withContext(Dispatchers.IO) {
            // Prepare encrypted params
            var serverTimestampDelta =
                CacheUtils.getObject<Long>(Constants.SLUG_SERVER_TIMESTAMP_DELTA)
            if (serverTimestampDelta == null) serverTimestampDelta = 0
            val deviceTimestamp = Instant.now().epochSecond
            val serverTimestamp = deviceTimestamp - serverTimestampDelta
            val hashServerTimestamp = "Ω${serverTimestamp}994".sha256()

            val hashUrl = when {
                floor(Math.random() * 10.0) % 2.0 == 0.0 -> "∑${url}${serverTimestamp + 3017 - 281 * 3 + 294 / 3}∆"
                else -> "©${url}${serverTimestamp + 4827 + 31 * 26 - 218 / 2}®"
            }.sha256()


            val finalHeaders = hashMapOf<String, String>().apply {
                putAll(getStandardHeaders())
                putAll(customHeaders)
                put("last-modify", serverTimestamp.toString())
                put("if-non-matched", hashServerTimestamp)
                put("if-matched", hashUrl)
            }

            val finalParams = customParams.asJsonObject.apply {
                getStandardParams().forEach { (key, value) ->
                    this.addProperty(key, value)
                }

                // Metadata
                CacheUtils.getObject<String>(Constants.SLUG_METADATA)?.let { metadata ->
                    add("metadata", JsonObject().apply {
                        addProperty("metadata", metadata)
                    })
                }
            }

            try {
                val jsonResult =
                    mApiService.postGeneric(getRandomEncryptedApiUrl(), finalHeaders, finalParams)
                val jsonObjectResponse = try {
                    jsonResult.asJsonObject
                } catch (e: IllegalStateException) {
                    null
                }

                // Save server timestamp delta if any
                val newServerTimestamp =
                    if (jsonObjectResponse?.has(KEY_TIMESTAMP) == true) jsonObjectResponse[KEY_TIMESTAMP]?.asLong else null
                newServerTimestamp?.let {
                    val newServerTimestampDelta = deviceTimestamp - it
                    CacheUtils.saveObject(
                        Constants.SLUG_SERVER_TIMESTAMP_DELTA,
                        newServerTimestampDelta
                    )
                }

                // Check error and retry if needed
                if (jsonObjectResponse?.has("\uD83C\uDF69") == true ||
                    jsonObjectResponse?.has("\uD83C\uDFA7") == true ||
                    jsonObjectResponse?.has("\uD83C\uDFAF") == true
                ) {
                    return@withContext if (retryOnEncryptionError)
                        postGeneric(url, customHeaders, customParams, type, false)
                    else {
                        ApiResult.failure(
                            Throwable(
                                message = jsonObjectResponse["\uD83C\uDFAF"]?.asString ?: ""
                            )
                        )
                    }
                }

                return@withContext ApiResultHandler.handleSuccess(jsonResult, type, mGson)
            } catch (e: Exception) {
                ApiResultHandler.handleException(e)
            }
        }
    }

    open suspend fun downloadGeneric(fileUrl: String): ApiResult<ResponseBody> {
        return try {
            ApiResult.success(mApiService.downloadGeneric(fileUrl))
        } catch (e: Exception) {
            ApiResultHandler.handleException(e)
        }
    }

    open suspend fun HashMap<String, String>.toRequestJson() = mGson.toJsonTree(this)

}